#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
#include <math.h>
	
float desvStd(estudiante curso[]){
	float media=0,varianza=0,desviacion=0;
	int i;

	//calculo la media
	for (i=0;i<24;i++){
		media=media+curso[i].prom;
	}
	media=media/24;
	//calculo la varianza
	for (i=0;i<24;i++){
		varianza = varianza+pow(curso[i].prom-media,2);
	}
	desviacion=pow(varianza/24,0.5);
	return desviacion;
}	
	
float menor(float prom[]){

	int i;

	int menor;
	menor = prom[0]; //Así empezamos a comparar
 	
		for (i=0; i<24; i++){
		    if (prom[i]< menor){
		    menor=prom[i];
		    }
		}
	return menor;
}	
	
float mayor(float prom[]){
	int i;

	int mayor;
	mayor = prom[0]; //Le asignamos el primer elemento del array

		for (i=0; i<24; i++){
		    if (prom[i]> mayor){
		    mayor=prom[i];
		    }
		}  
	return mayor;
}	

void registroCurso(estudiante curso[]){
	//debe registrar las calificaciones
	int i=0;
	for(i=0;i<24;i++){
		printf("\ningrese las notas de el estudiante %s %s %s\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM);
		
		//notas proyectos
		printf("ingrese nota proyecto 1: \n");
		scanf("%f",&curso[i].asig_1.proy1);
		printf("ingrese nota proyecto 2: \n");
		scanf("%f",&curso[i].asig_1.proy2);
		printf("ingrese nota proyecto 3: \n");
		scanf("%f",&curso[i].asig_1.proy3);

		//notas controles
		printf("ingrese nota control 1: \n");
		scanf("%f",&curso[i].asig_1.cont1);
		printf("ingrese nota control 2: \n");
		scanf("%f",&curso[i].asig_1.cont2);
		printf("ingrese nota control 3: \n");
		scanf("%f",&curso[i].asig_1.cont3);
		printf("ingrese nota control 4: \n");
		scanf("%f",&curso[i].asig_1.cont4);
		printf("ingrese nota control 5: \n");
		scanf("%f",&curso[i].asig_1.cont5);
		printf("ingrese nota control 6: \n");
		scanf("%f",&curso[i].asig_1.cont6);

		//promedio
		curso[i].prom=(curso[i].asig_1.proy1+curso[i].asig_1.proy2+curso[i].asig_1.proy3+curso[i].asig_1.cont1+curso[i].asig_1.cont2+curso[i].asig_1.cont3+curso[i].asig_1.cont4+curso[i].asig_1.cont5+curso[i].asig_1.cont6)/9;

		printf("el promedio es %f\n",curso[i].prom );
	}
}

void clasificarEstudiantes(char path[], estudiante curso[]){
	int i,j;
	estudiante temp;
	FILE *fileAprob;
	FILE *fileReprob;

	if((fileAprob=fopen("aprobados.txt","w"))==NULL){
		printf("\nerror al crear el archivo");
		exit(0); 
	}

	if((fileReprob=fopen("reprobados.txt","w"))==NULL){
		printf("\nerror al crear el archivo");
		exit(0); 
	}

	for (i=1;i<24;i++){
		for (j=0;j<24-1;j++){
			if (curso[j].prom<curso[j+1].prom){
				temp=curso[j];
				curso[j]=curso[j+1];
				curso[j+1]=temp;
			}
		}
	}

	for (int i = 0; i<24; i++){
		if (curso[i].prom>=4){
			fprintf(fileAprob,"%s\t%s\t%s\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t %.1f\n", 
				curso[i].nombre,
				curso[i].apellidoP,
				curso[i].apellidoM,
				curso[i].asig_1.proy1,
				curso[i].asig_1.proy2,
				curso[i].asig_1.proy3,
				curso[i].asig_1.cont1,
				curso[i].asig_1.cont2,
				curso[i].asig_1.cont3,
				curso[i].asig_1.cont4,
				curso[i].asig_1.cont5,
				curso[i].asig_1.cont6,
				curso[i].prom);
		}else{
			fprintf(fileReprob,"%s\t%s\t%s\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t %.1f\n", 
				curso[i].nombre,
				curso[i].apellidoP,
				curso[i].apellidoM,
				curso[i].asig_1.proy1,
				curso[i].asig_1.proy2,
				curso[i].asig_1.proy3,
				curso[i].asig_1.cont1,
				curso[i].asig_1.cont2,
				curso[i].asig_1.cont3,
				curso[i].asig_1.cont4,
				curso[i].asig_1.cont5,
				curso[i].asig_1.cont6,
				curso[i].prom);
		}	
	}
	fclose(fileAprob);
	fclose(fileReprob);

	printf("archivos grabados exitosamente\n");
	/*
	for (i=0;i<24;i++){
		printf("%s,%f:\n",curso[i].nombre,curso[i].prom);
	}*/

}
void metricasEstudiantes(estudiante curso[]){
	int i;
	float prom[24];
	float prommayor,prommenor,desviacion;
	//recorrer el arreglo de estudiante
	for(i=0;i<24;i++){
		//llenar arreglo de flotante con promedios;
		prom[i]=curso[i].prom;
	}
	//llamar funcion mejor promedio
	prommayor=mayor(prom);
	//llamar funcion promedio mas bajo
	prommenor=menor(prom);
	//llamar funcion desviacion estandar
	desviacion=desvStd(curso);
	//mostrar por pantalla los resultados
	printf("el promedio mas alto es %f, el promedio mas bajo es %f, la deesviacion estandar es %f\n",prommayor,prommenor,desviacion ); 
}





void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("destino", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}

int main(){
	estudiante curso[24]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}